## Demo Vue + vite + typescript

# Description

   The application supports making simple tests.
   With each implementation, the set of questions will be randomly given.
   Before taking the test need to enter your name.
   Once done, you can review the correct answer and try again

# Install and run project 

   1. Clone project from gitlab

         Step 1: From a folder, right click and open Git Bash

         Step 2: Connect to gitlab account with two command lines
               git config --global user.name "Your GitLab Username"
               git config --global user.email "your-email@example.com"

         Step 3: Clone project
               In Git Bash, uses this command to clone project  from gitlab
               git clone [repository_url]

   2. Open the folder you just cloned from gitlab in the IDE

   3. Setup json-server: cd server in terminal
      use "npm install -g json-server" to install json-server
      
      use "json-server --watch db.json" to run server

   4. Use command in terminal: "npm install" to dowload library in file package.json

   5. Use command in terminal: "npm run dev" to run application

