import axios from 'axios';
import {ref} from "vue";

const BASE_URL = 'http://localhost:3000';

export const fetchSomeData = async (): Promise<any> =>{
    let data ;
    try {
        const response = await axios.get(`${BASE_URL}/questions`);
        data =response.data;
        return response.data;
    } catch (error) {
        throw error;
    }
};


