type Question = {
    question:string,
    answers:number[],
    correctAnswer:number,
    status: number
};
